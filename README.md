# Temperature Controlled Cooling System

## Goal

The goal is to create a software for the PIC18F46K20 that controls a PWM fan with a control loop. 
This control loop get's a temperature measurment from a temperature sensor and adjusts the fan speed over the pwm signal. 
The measured temeperature and the dutycycle of the pwm signal will sent over the UART to a Terminal.

## Must points

- [x] Control a pwm fan over pwm
- [x] Get the temperature from a temperature sensor
- [x] Control the pwm fan with a control loop with the temperature data
- [x] Send the measured temperature over the UART
- [x] Send the dutycycle of the pwm signal over the UART

## Optional points

- [x] Read the "Tacho" signal from the pwm fan
- [ ] Control the fanspeed with the input of the "Tacho" signal to a stable rpm **(Not nessesary!!!)**