//------------------------------------------------------------------------------
//  File name:      calculation.c
//
//  Description:
//  This file contains all the functions to calculate values, that are needed.
//  
//  Author:         Fabian Bachofen
//------------------------------------------------------------------------------

//--- Libraries ----------------------------------------------------------------
#include <xc.h>

#include "global.h"

//--- Functions ----------------------------------------------------------------

//--- Calculate Fan Speed --------------------------------------------------
unsigned short CALC_fan_rpm(unsigned short tacho_count)
{
    // This functions parameter "tacho_count" has to contain the value 
    // from a measurement duration of 1 second.
    
    // fan speed [rpm] = frequency [Hz] � 60 � 2
    // The same result can be achieved much easier:
    // fan speed [rpm] = frequency [Hz] * 30
    
    return tacho_count * 30;
}

//--- Calculate NTC Temperature --------------------------------------------
float CALC_ntc_temperature(float ntc_resistance, float ntc_nom_resistance, float ntc_beta)
{
    float temperature = 0;
    
    temperature = (298.15f/(1-(298.15f/ntc_beta)*log(ntc_nom_resistance/ntc_resistance))-273.15f);
    
    return temperature;
}

//--- Calculate average (unsigned short) -----------------------------------
unsigned short CALC_average(unsigned short value_1, unsigned short value_2)
{
    return (unsigned short)(value_1 + value_2) / 2;
}

//--- Calculate PWM in % ---------------------------------------------------
unsigned char CALC_pwm_in_percent(unsigned short max_pwm_value, unsigned short pwm_value)
{
    return (unsigned char)((100.0f / max_pwm_value) * pwm_value);
}

//--- Calculate NTC Resistance ---------------------------------------------
float CALC_ntc_resistance(float r_pull, unsigned short max_adc_value, unsigned short adc_value)
{
    float u_pd  = 0;
    float r_ntc = 0;
    
    u_pd  = (float)((10000 / max_adc_value) * adc_value);       // calculate ntc voltage
    r_ntc = (float)((10000 - u_pd) / (u_pd / r_pull));          // calculate ntc resistance
    
    return r_ntc;
}

//--- Calculate PI Regulator -----------------------------------------------
float CALC_pid_regulatror(unsigned char channel, float set_temperature, float actual_temperature, float ambient_temperature)
{    
    float           error               = 0;
    float           kp_error            = 0;
    float           kd_error            = 0;
    
    float           pid                 = 0;
    
    static float    previose_error_1    = 0;
    static float    ki_error_1          = 0;

    static float    previose_error_2    = 0;
    static float    ki_error_2          = 0;
    
    if(actual_temperature >= ambient_temperature)   // actual temperature must be bigger then than the set temperature
    {
        switch(channel)
        {
            case 1: //--- Error Calculation --------------------------------------------
                    error = actual_temperature - set_temperature;

                    //--- KP -----------------------------------------------------------
                    kp_error = PID_KP * error;

                    //--- KI -----------------------------------------------------------
                    ki_error_1 += PID_KI * error;

                    if(ki_error_1 > PID_MAX_VALUE)
                    {
                        ki_error_1 = PID_MAX_VALUE;
                    }

                    //--- KD -----------------------------------------------------------
                    kd_error = PID_KD * (error - previose_error_1);

                    //--- Set previous Error -------------------------------------------
                    previose_error_1 = error;

                    pid = kp_error + ki_error_1 + kd_error;

                    if(pid > PID_MAX_VALUE)
                    {
                        pid = PID_MAX_VALUE;
                    }
                    if(pid < 0)
                    {
                        pid = 0;
                    }
                break;

            case 2: //--- Error Calculation --------------------------------------------
                    error = actual_temperature - set_temperature;

                    //--- KP -----------------------------------------------------------
                    kp_error = PID_KP * error;

                    //--- KI -----------------------------------------------------------
                    ki_error_2 += PID_KI * error;

                    if(ki_error_2 > PID_MAX_VALUE)
                    {
                        ki_error_2 = PID_MAX_VALUE;
                    }

                    //--- KD -----------------------------------------------------------
                    kd_error = PID_KD * (error - previose_error_2);

                    //--- Set previose Error -------------------------------------------
                    previose_error_2 = error;

                    pid = kp_error + ki_error_2 + kd_error;

                    if(pid > PID_MAX_VALUE)
                    {
                        pid = PID_MAX_VALUE;
                    }
                    if(pid < 0)
                    {
                        pid = 0;
                    }
                break;

            default: pid = 0;
                break;
        } // <-- end of switch
    }
    else
    {
        switch(channel)
        {
            case 1: previose_error_1    = 0;
                    ki_error_1          = 0;
                break;
                
            case 2: previose_error_2    = 0;
                    ki_error_2          = 0;
                break;
        }
    }
   
    return pid;
}

// <--- end of file