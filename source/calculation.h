//------------------------------------------------------------------------------
//  File name:      calculation.h
//  
//  Description:
//  This file contains all the definitions and function declarations of the 
//  calculations needed
//  
//  Author:         Fabian Bachofen
//------------------------------------------------------------------------------

#ifndef CALCULATION_H
#define	CALCULATION_H

//--- Libraries ----------------------------------------------------------------
#include <xc.h>

#include "global.h"

//--- Definitions --------------------------------------------------------------

//--- NTC Temperature Sensor -----------------------------------------------
#define         NTC_BETA_AMBIENT         3630.0f
#define         NTC_R_NOM_AMBIENT       10000.0f
#define         NTC_PULLDOWN_AMBIENT    10000.0f

#define         NTC_BETA_TEMP_1          3630.0f
#define         NTC_R_NOM_TEMP_1        10000.0f
#define         NTC_PULLDOWN_TEMP_1     10000.0f

#define         NTC_BETA_TEMP_2          3630.0f
#define         NTC_R_NOM_TEMP_2        10000.0f
#define         NTC_PULLDOWN_TEMP_2     10000.0f

//--- PI Regulator ---------------------------------------------------------
#define         PID_KP                     30.0f
#define         PID_KI                      7.0f
#define         PID_KD                     15.0f

#define         PID_MAX_VALUE      MAX_PWM_VALUE

//--- Variables and Arrays -----------------------------------------------------


//--- Function Declarations ----------------------------------------------------
unsigned short  CALC_fan_rpm            (unsigned short tacho_count);

float           CALC_ntc_temperature    (float ntc_resistance, 
                                         float ntc_nom_resistance, 
                                         float ntc_beta);

unsigned short  CALC_average            (unsigned short value_1, 
                                         unsigned short value_2);

unsigned char   CALC_pwm_in_percent     (unsigned short max_pwm_value,
                                         unsigned short pwm_value);

float           CALC_ntc_resistance     (float          r_pullup, 
                                         unsigned short max_adc_value, 
                                         unsigned short adc_value);

float           CALC_pid_regulatror     (unsigned char channel,
                                         float set_temperature, 
                                         float actual_temperature,
                                         float ambient_temperature);

#endif	/* CALCULATION_H */

// <--- end of file