//------------------------------------------------------------------------------
//  File name:      convert.c
//
//  Description:
//  This file contains all the functions to convert values, that are needed.
//  
//  Author:         Fabian Bachofen
//------------------------------------------------------------------------------

//--- Libraries ----------------------------------------------------------------
#include <xc.h>

#include "global.h"

//--- Functions ----------------------------------------------------------------

//--- Convert unsigned short to string -------------------------------------
void CONVERT_ushort_to_str(unsigned short value)
{
    unsigned char index = 0;
    
    convert_output_str[index] = 0x30;

    for(;value >= 10000; value -= 10000)
    {
        convert_output_str[index] ++;
    }
    index ++;

    convert_output_str[index] = 0x30;

    for(;value >= 1000; value -= 1000)
    {
        convert_output_str[index] ++;
    }
    index ++;

    convert_output_str[index] = 0x30;

    for(;value >= 100; value -= 100)
    {
        convert_output_str[index] ++;
    }
    index ++;

    convert_output_str[index] = 0x30;

    for(;value >= 10; value -= 10)
    {
        convert_output_str[index] ++;
    }
    index ++;

    convert_output_str[index] = 0x30;

    for(;value >= 1; value -= 1)
    {
        convert_output_str[index] ++;
    }
    index ++;
    
    convert_output_str[index] = 0x00;
}

//--- Convert float to string ----------------------------------------------
void CONVERT_float_to_str(float value)
{
    unsigned char index = 0;
    
    convert_output_str[index] = 0x30;

    for(;value >= 10000; value -= 10000)
    {
        convert_output_str[index] ++;
    }
    index ++;

    convert_output_str[index] = 0x30;

    for(;value >= 1000; value -= 1000)
    {
        convert_output_str[index] ++;
    }
    index ++;

    convert_output_str[index] = 0x30;

    for(;value >= 100; value -= 100)
    {
        convert_output_str[index] ++;
    }
    index ++;

    convert_output_str[index] = 0x30;

    for(;value >= 10; value -= 10)
    {
        convert_output_str[index] ++;
    }
    index ++;

    convert_output_str[index] = 0x30;

    for(;value >= 1; value -= 1)
    {
        convert_output_str[index] ++;
    }
    index ++;
    
    convert_output_str[index] = 0x2E;
    
    index ++;
    
    convert_output_str[index] = 0x30;

    for(;value >= 0.1; value -= 0.1)
    {
        convert_output_str[index] ++;
    }
    index ++;
    
    convert_output_str[index] = 0x30;

    for(;value >= 0.1; value -= 0.1)
    {
        convert_output_str[index] ++;
    }
    index ++;
    
    convert_output_str[index] = 0x00;
}

//--- Convert PWM to LEDs --------------------------------------------------
void CONVERT_pwm_to_led(unsigned char pwm_module, unsigned char pwm_percent_value)
{
    switch(pwm_module)
    {
        //--- LED for PWM 1 --------------------------------------------
        case 1:		if(pwm_percent_value != 0)
					{
						if((pwm_percent_value < 25) && (pwm_percent_value >= 1))
						{
							LED_PWM1_25 	= (__bit)(on_off_clock);
							LED_PWM1_50		= 0;
							LED_PWM1_75		= 0;
							LED_PWM1_100	= 0;
						}
						if((pwm_percent_value < 50) && (pwm_percent_value >= 25))
						{
							LED_PWM1_25 	= 1;
							LED_PWM1_50 	= (__bit)(on_off_clock);
							LED_PWM1_75		= 0;
							LED_PWM1_100	= 0;
						}
						if((pwm_percent_value < 75) && (pwm_percent_value >= 50))
						{
							LED_PWM1_25 	= 1;
							LED_PWM1_50 	= 1;
							LED_PWM1_75 	= (__bit)(on_off_clock);
							LED_PWM1_100	= 0;
						}
						if((pwm_percent_value < 100) && (pwm_percent_value >= 75))
						{
							LED_PWM1_25 	= 1;
							LED_PWM1_50 	= 1;
							LED_PWM1_75 	= 1;
							LED_PWM1_100	= (__bit)(on_off_clock);
						}
						if(pwm_percent_value >= 100)
						{
							LED_PWM1_25 	= 1;
							LED_PWM1_50 	= 1;
							LED_PWM1_75 	= 1;
							LED_PWM1_100	= 1;
						}
					}
					else
					{
						LED_PWM1_25 	= 0;
						LED_PWM1_50 	= 0;
						LED_PWM1_75 	= 0;
						LED_PWM1_100 	= 0;
					}
            break;
            
        //--- LED for PWM 2 --------------------------------------------
        case 2:		if(pwm_percent_value != 0)
					{
						if((pwm_percent_value < 25) && (pwm_percent_value >= 1))
						{
							LED_PWM2_25 	= (__bit)(on_off_clock);
							LED_PWM2_50		= 0;
							LED_PWM2_75		= 0;
							LED_PWM2_100	= 0;
						}
						if((pwm_percent_value < 50) && (pwm_percent_value >= 25))
						{
							LED_PWM2_25 	= 1;
							LED_PWM2_50 	= (__bit)(on_off_clock);
							LED_PWM2_75		= 0;
							LED_PWM2_100	= 0;
						}
						if((pwm_percent_value < 75) && (pwm_percent_value >= 50))
						{
							LED_PWM2_25 	= 1;
							LED_PWM2_50 	= 1;
							LED_PWM2_75 	= (__bit)(on_off_clock);
							LED_PWM2_100	= 0;
						}
						if((pwm_percent_value < 100) && (pwm_percent_value >= 75))
						{
							LED_PWM2_25 	= 1;
							LED_PWM2_50 	= 1;
							LED_PWM2_75 	= 1;
							LED_PWM2_100	= (__bit)(on_off_clock);
						}
						if(pwm_percent_value >= 100)
						{
							LED_PWM2_25 	= 1;
							LED_PWM2_50 	= 1;
							LED_PWM2_75 	= 1;
							LED_PWM2_100	= 1;
						}
					}
					else
					{
						LED_PWM2_25 	= 0;
						LED_PWM2_50 	= 0;
						LED_PWM2_75 	= 0;
						LED_PWM2_100 	= 0;
					}
            break;
		//--- Default --------------------------------------------------
        default:
            break;
    }
}

//--- on/off clock ---------------------------------------------------------
void change_on_off_clock(void)
{
    on_off_clock = !on_off_clock;
}

// <--- end of file