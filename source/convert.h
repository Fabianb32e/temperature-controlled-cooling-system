//------------------------------------------------------------------------------
//  File name:      convert.h
//  
//  Description:
//  This file contains all the definitions and function declarations of the 
//  conversion needed
//  
//  Author:         Fabian Bachofen
//------------------------------------------------------------------------------

#ifndef CONVERT_H
#define	CONVERT_H

//--- Libraries ----------------------------------------------------------------
#include <xc.h>

#include "global.h"

//--- Definitions --------------------------------------------------------------

//--- Variables and Arrays -----------------------------------------------------
char            convert_output_str [10];

unsigned char   on_off_clock        = 0;

//--- Function Declarations ----------------------------------------------------
void            CONVERT_ushort_to_str           (unsigned short value);

void            CONVERT_float_to_str            (float value);

void            CONVERT_pwm_to_led              (unsigned char pwm_module, 
                                                 unsigned char pwm_percent_value);

void            change_on_off_clock             (void);

#endif	/* CONVERT_H */

// <--- end of file