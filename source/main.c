//------------------------------------------------------------------------------
//  File name:      main.c
//
//  Author:         Fabian Bachofen
//------------------------------------------------------------------------------

//--- Libraries ------------------------------------------------------------
#include <xc.h>

#include "global.h"

//--- Global Variables ---------------------------------------------
unsigned short  timer_3_counter_0       = 0;
unsigned char   update                  = 0;

//--- Main Program ---------------------------------------------------------
void main(void) 
{
    //--- Local Variables ----------------------------------------------
    unsigned char   pwm1_value_percent              = 0;
    unsigned char   pwm2_value_percent              = 0;
    
    unsigned short  fan1_tacho                      = 0;
    unsigned short  fan1_rpm                        = 0;
    
    unsigned short  fan2_tacho                      = 0;
    unsigned short  fan2_rpm                        = 0;
    
    unsigned short  adc_temp_ambient                = 0;
    unsigned short  average_adc_temp_ambient        = 0;
    
    unsigned short  adc_temp_1                      = 0;
    unsigned short  adc_temp_2                      = 0;
    unsigned short  average_adc_temp_1              = 0;
    unsigned short  average_adc_temp_2              = 0;
    
    unsigned short  adc_target_temp_1               = 0;
    unsigned short  adc_target_temp_2               = 0;
    unsigned short  average_adc_target_temp_1       = 0;
    unsigned short  average_adc_target_temp_2       = 0;
    unsigned short  target_temperature_1            = 0;
    unsigned short  target_temperature_2            = 0;
    
    float           resistance_ntc_temp_ambient     = 0;
    float           resistance_ntc_temp_1           = 0;
    float           resistance_ntc_temp_2           = 0;
    
    float           temp_ambient                    = 0;
    float           temp_1                          = 0;
    float           temp_2                          = 0;
    
    float           pid_regulator_value_1           = 0;
    float           pid_regulator_value_2           = 0;
    
    //--- Initialization -----------------------------------------------
    CLOCK_init();       // initialize the MCU clock
    PORT_init();        // initialize the MCU ports
    ISR_init();         // initialize the MCU interrupts
    
    //--- UART Initialization --------------------------------------
    UART_init();        // initialize the MCU UART port
    __delay_ms(500);    // 500 ms delay
    //--- Get UART stable --------------------------------------
    strcpy(tx_buffer, "                            \r\0");
    UART_send();
    UART_send();
    UART_send();
    UART_send();
    strcpy(tx_buffer, "Finished UART Initialization\r\0");
    UART_send();
    
    //--- ADC Initialization ---------------------------------------
    ADC_init();         // initialize the MCU ADC
    strcpy(tx_buffer, "Finished ADC Initialization\r\0");
    UART_send();
    
    //--- PWM Initialization --------------------------------------
    PWM_init();         // initialize the MCU PWM module
    PWM_set(1, 160);    // PWM 1 set to 25%
    PWM_set(2, 160);    // PWM 2 set to 25%
    strcpy(tx_buffer, "Finished PWM Initialization\r\0");
    UART_send();
    
    //--- Timer Initialization -------------------------------------
    TIMER0_init();      // Timer 0 used for Tacho signal of Fan 1
    TIMER1_init();      // Timer 1 used for Tacho signal of Fan 2
    TIMER3_init();      // Timer 3 used for update interval
    strcpy(tx_buffer, "Finished Timer Initialization\r\0");
    UART_send();
    
    //--- Finished with Initialization ---------------------------------    
    strcpy(tx_buffer, "Finished MCU Initialization\r\0");
    UART_send();
    
    LED_INIT = 1;       // Set the init LED to show, that the initialization is finished
    
    TIMER_start();      // start all timers at once (Timer 0, Timer 1 and Timer 3)
    
    //--- Endless Loop -------------------------------------------------
    while(1)
    {
        WATCHDOG_clear();       // clear the watchdog timer

        if(update == 1)
        {
            LED_USER_1 = 1;     // turn on the user 1 LED
            
            strcpy(tx_buffer, "--------- START -----------\r\0");
            UART_send();
            //--- Fan Tacho Readout --------------------------------
            
            //--- Fan 1 ----------------------------------------
            fan1_tacho = TIMER0_read();             // Read the tacho value for the fan 1
            fan1_rpm = CALC_fan_rpm(fan1_tacho);    // Calculate the rpm from the fan 1
            
            //--- Send Tacho Frequency ----------------------
            CONVERT_ushort_to_str(fan1_tacho);
            strcpy(tx_buffer, "Fan 1         : ");
            strcat(tx_buffer, convert_output_str);
            strcat(tx_buffer, " Hz\r\0");
            UART_send();
            
            //--- Send Tacho RPM ---------------------------
            CONVERT_ushort_to_str(fan1_rpm);
            strcpy(tx_buffer, "Fan 1         : ");
            strcat(tx_buffer, convert_output_str);
            strcat(tx_buffer, " RPM\r\0");
            UART_send();
            
            //--- Fan 2 ----------------------------------------
            fan2_tacho = TIMER1_read();             // Read the tacho value for the fan 2
            fan2_rpm = CALC_fan_rpm(fan2_tacho);    // Calculate the rpm from the fan 2
            
            //--- Send Tacho Frequency ----------------------
            CONVERT_ushort_to_str(fan2_tacho);
            strcpy(tx_buffer, "Fan 2         : ");
            strcat(tx_buffer, convert_output_str);
            strcat(tx_buffer, " Hz\r\0");
            UART_send();
            
            //--- Send Tacho RPM ---------------------------
            CONVERT_ushort_to_str(fan2_rpm);
            strcpy(tx_buffer, "Fan 2         : ");
            strcat(tx_buffer, convert_output_str);
            strcat(tx_buffer, " RPM\r\0");
            UART_send();
            
            //--- Callculate the Temperature -----------------------
            resistance_ntc_temp_ambient = CALC_ntc_resistance(NTC_PULLDOWN_AMBIENT, MAX_ADC_VALUE, average_adc_temp_ambient);
            resistance_ntc_temp_1       = CALC_ntc_resistance(NTC_PULLDOWN_TEMP_1,  MAX_ADC_VALUE, average_adc_temp_1);
            resistance_ntc_temp_2       = CALC_ntc_resistance(NTC_PULLDOWN_TEMP_2,  MAX_ADC_VALUE, average_adc_temp_2);
            
            temp_ambient                = CALC_ntc_temperature(resistance_ntc_temp_ambient, NTC_R_NOM_AMBIENT, NTC_BETA_AMBIENT);
            temp_1                      = CALC_ntc_temperature(resistance_ntc_temp_1,       NTC_R_NOM_TEMP_1,  NTC_BETA_TEMP_1);
            temp_2                      = CALC_ntc_temperature(resistance_ntc_temp_2,       NTC_R_NOM_TEMP_2,  NTC_BETA_TEMP_2);
            
            //--- Set Target Temnperature 1 ------------------------
            target_temperature_1    = average_adc_target_temp_1 / 10;       // calculate the target temperature for control loop 1
            
            if(target_temperature_1 > 100)
            {
                target_temperature_1 = 100;
            }
            
            if(target_temperature_1 < (unsigned char)(temp_ambient))
            {
                target_temperature_1 = (unsigned char)(temp_ambient);
            }
            
            //--- Set Target Temnperature 2 ------------------------
            target_temperature_2    = average_adc_target_temp_2 / 10;       // calculate the target temperature for control loop 2
            
            if(target_temperature_2 > 100)
            {
                target_temperature_2 = 100;
            }
            
            if(target_temperature_2 < (unsigned char)(temp_ambient))
            {
                target_temperature_2 = (unsigned char)(temp_ambient);
            }
            
            //--- Send Target Temperature 1 --------------------
            CONVERT_ushort_to_str(target_temperature_1);
            strcpy(tx_buffer, "Target Temp 1 : ");
            strcat(tx_buffer, convert_output_str);
            strcat(tx_buffer, " deg C\r\0");
            UART_send();
            
            //--- Send Target Temperature 2 --------------------
            CONVERT_ushort_to_str(target_temperature_2);
            strcpy(tx_buffer, "Target Temp 2 : ");
            strcat(tx_buffer, convert_output_str);
            strcat(tx_buffer, " deg C\r\0");
            UART_send();
            
            //--- Send Ambient Temperature ---------------------
            CONVERT_float_to_str(temp_ambient);
            strcpy(tx_buffer, "Ambient Temp  : ");
            strcat(tx_buffer, convert_output_str);
            strcat(tx_buffer, " deg C\r\0");
            UART_send();
            
            //--- Send Object 1 Temperature -------------------
            CONVERT_float_to_str(temp_1);
            strcpy(tx_buffer, "Object 1 Temp : ");
            strcat(tx_buffer, convert_output_str);
            strcat(tx_buffer, " deg C\r\0");
            UART_send();
            
            //--- Send Object 2 Temperature -------------------
            CONVERT_float_to_str(temp_2);
            strcpy(tx_buffer, "Object 2 Temp : ");
            strcat(tx_buffer, convert_output_str);
            strcat(tx_buffer, " deg C\r\0");
            UART_send();
            
            //--- Reset average values ------------------------
            average_adc_temp_ambient    = 0;
            average_adc_temp_1          = 0;
            average_adc_temp_2          = 0;
            average_adc_target_temp_1   = 0;
            average_adc_target_temp_2   = 0;
            
            //--- PID Regulator -------------------------------------
            
            //--- Fan 1 ----------------------------------------
            pid_regulator_value_1 = CALC_pid_regulatror(1, target_temperature_1, temp_1, temp_ambient);
            
            if(pid_regulator_value_1 > MAX_PWM_VALUE)
            {
                pid_regulator_value_1 = MAX_PWM_VALUE;
                PWM_set(1, MAX_PWM_VALUE);
            }
            else if(pid_regulator_value_1 < MIN_PWM_VALUE)
            {
                pid_regulator_value_1 = MIN_PWM_VALUE;
                PWM_set(1, MIN_PWM_VALUE);
            }
            else
            {            
                PWM_set(1, (unsigned short)(pid_regulator_value_1));
            }
            
            //--- Fan 2 ----------------------------------------
            pid_regulator_value_2 = CALC_pid_regulatror(2, target_temperature_2, temp_2, temp_ambient);
            
            if(pid_regulator_value_2 > MAX_PWM_VALUE)
            {
                pid_regulator_value_2 = MAX_PWM_VALUE;
                PWM_set(2, MAX_PWM_VALUE);
            }
            else if(pid_regulator_value_2 < MIN_PWM_VALUE)
            {
                pid_regulator_value_2 = MIN_PWM_VALUE;
                PWM_set(2, MIN_PWM_VALUE);
            }
            else
            {
                PWM_set(2, (unsigned short)(pid_regulator_value_2));
            }
            
            //--- Send PID-Value 1 -----------------------------
            CONVERT_float_to_str(pid_regulator_value_1);
            strcpy(tx_buffer, "PID-Value 1   : ");
            strcat(tx_buffer, convert_output_str);
            strcat(tx_buffer, "\r\0");
            UART_send();
            
            //--- Send PID-Value 2 -----------------------------
            CONVERT_float_to_str(pid_regulator_value_2);
            strcpy(tx_buffer, "PID-Value 2   : ");
            strcat(tx_buffer, convert_output_str);
            strcat(tx_buffer, "\r\0");
            UART_send();
            
            //--- PWM1 LEDs ---------------------------------------------
            pwm1_value_percent = CALC_pwm_in_percent(MAX_PWM_VALUE, (unsigned short)(pid_regulator_value_1));
            CONVERT_pwm_to_led(1, pwm1_value_percent);
            
            CONVERT_ushort_to_str(pwm1_value_percent);
            strcpy(tx_buffer, "PWM Fan 1     : ");
            strcat(tx_buffer, convert_output_str);
            strcat(tx_buffer, " %\r\0");
            UART_send();
            
            //--- PWM2 LEDs ---------------------------------------------
            pwm2_value_percent = CALC_pwm_in_percent(MAX_PWM_VALUE, (unsigned short)(pid_regulator_value_2));
            CONVERT_pwm_to_led(2, pwm2_value_percent);
            
            CONVERT_ushort_to_str(pwm2_value_percent);
            strcpy(tx_buffer, "PWM Fan 2     : ");
            strcat(tx_buffer, convert_output_str);
            strcat(tx_buffer, " %\r\0");
            UART_send();
            
            strcpy(tx_buffer, "---------- END ------------\r\0");
            UART_send();
            
            update = 0;         // reset the update flag
            
            LED_USER_1 = 0;     // turn off the user 1 LED
            
            TIMER_start();      // start all timer at once
        }
        else
        {
            //--- Read the ADC -----------------------------------------
            adc_temp_ambient            = ADC_read(TEMP_AMBIENT);
            adc_temp_1                  = ADC_read(TEMP_OBJECT_1);
            adc_temp_2                  = ADC_read(TEMP_OBJECT_2);
            adc_target_temp_1           = ADC_read(TEMP_SET_1);
            adc_target_temp_2           = ADC_read(TEMP_SET_2);
            
            //--- Calculate average ADC value --------------------------
            average_adc_temp_ambient    = CALC_average(average_adc_temp_ambient,  adc_temp_ambient);
            average_adc_temp_1          = CALC_average(average_adc_temp_1,        adc_temp_1);
            average_adc_temp_2          = CALC_average(average_adc_temp_2,        adc_temp_2);
            average_adc_target_temp_1   = CALC_average(average_adc_target_temp_1, adc_target_temp_1);
            average_adc_target_temp_2   = CALC_average(average_adc_target_temp_2, adc_target_temp_2);
        }
    }
}

//--- Interrupt Service Routine --------------------------------------------
void __interrupt() ISR(void)
{
    //--- Timer 3 ------------------------------------------------------   
    if(TMR3IF == 1)
    {
        GIE     = 0;
        TMR3ON  = 0;
        TIMER3_reset();
        
        if(timer_3_counter_0 <= 117)
        {
            timer_3_counter_0 ++;
            TMR3ON = 1;
        }
        else
        {
            timer_3_counter_0   = 0;
            change_on_off_clock();
            update              = 1;
        }
        
        TMR3IF  = 0;
        GIE     = 1;
    }
    
    //--- UART Receive -------------------------------------------------
    if(RCIF == 1)
    {
        // not implemented yet...
        // RCIF is disabled
    }
}

// <--- end of file