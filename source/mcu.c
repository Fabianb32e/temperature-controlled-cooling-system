//------------------------------------------------------------------------------
//  File name:      mcu.c
//
//  Description:
//  This file contains all the functions to control its hardware.
//  Example: Timer, Watchdog, UART, etc...
//  
//  Author:         Fabian Bachofen
//------------------------------------------------------------------------------

//--- Libraries ----------------------------------------------------------------
#include <xc.h>

#include "global.h"

//--- Functions ----------------------------------------------------------------

//--- Clock Initialization -------------------------------------------------
void CLOCK_init(void)
{
    OSCCON  = 0b01111000;       // 16 MHz (HFINTOSC), clock from FOSC<2:0> of CONFIG1 Register
    OSCTUNE = 0b11000000;       // INTSRC from the HFINTOSC, PLL enabled
    
    while(IOFS != 1);           // Wait for the HFINTOSC frequency to be stable.
}

//--- Watchdog Clear -------------------------------------------------------
void WATCHDOG_clear(void)
{
    CLRWDT();       // Clear Watchdog-Timer
}

//--- Port Initialization---------------------------------------------------
void PORT_init(void)
{
    PORTA   = 0b00000000;
    TRISA   = 0b00111111;       // bit 1 - 2 for ADC, bit 5 for Timer 0, bit 6 for ADC
    LATA    = 0b00000000;
    
    PORTB   = 0b00000000;
    TRISB   = 0b00000000;
    LATB    = 0b00000000;
    
    PORTC   = 0b00000000;
    TRISC   = 0b11000001;       // bit 1 for Timer 1, bit 6 - 7 for UART-Port
    LATC    = 0b00000000;
    
    PORTD   = 0b00000000;
    TRISD   = 0b00000000;
    LATD    = 0b00000000;
    
    PORTE   = 0b00000000;
    TRISE   = 0b00000000;
    LATE    = 0b00000000;
}

//--- Interrupt Initialization ---------------------------------------------
void ISR_init(void)
{
    IPEN    = 0;    // Interrupt Priority is set to 0 (disabled)
    GIE     = 1;    // Global     Interrupt Enable set to 1
    PEIE    = 1;    // Peripheral Interrupt Enable set to 1
}

//--- UART Initialization --------------------------------------------------
void UART_init(void)
{
    TXSTA   = 0b00100000;
    RCSTA   = 0b10010000;
    BAUDCON = 0b00000000;
    
    // Baudrate Callculation:
    // SPBRG = (FOSC / (desired Baudrate * 64)) - 1
    // Error = (callculated Baudrate - desired Baudrate) / desired Baudrate
    // The Error has to be less than 5% in order to work right!
    
    SPBRG   = 103;  // 9600 baud
    
    RCIE    = 0;    // Receive  Interrupt Enable is disabled
    RCIP    = 0;    // Set Interrupt to low priority
    TXIE    = 0;    // Transmit Interrupt Enable is diabled
    TXIP    = 0;    // Set Interrupt to low priority
}

//--- UART Send ------------------------------------------------------------
void UART_send(void)
{  
    unsigned char index = 0;
    
    while((tx_buffer[index] != 0x00) && (index < sizeof(tx_buffer)))
    {
        while(TXIF == 0);               // wait until the character was sent
        TXREG = tx_buffer[index];       // write character value to the TX register
        index++;                        // go to the next index of the buffer
    }
}

//--- ADC Initialization ---------------------------------------------------
void ADC_init(void)
{
    ADCON0  = 0b00000001;   // ADC enabled
    ADCON1  = 0b00000000;   // Set the reference to VSS & VDD
    ADCON2  = 0b10111110;   // right justified, 20TAD, FOSC/64
}

//--- ADC Read -------------------------------------------------------------
unsigned short ADC_read(unsigned char channel)
{
    unsigned short adc_value    = 0;
    
    if(channel <= MAX_ADC_CHANNELS)
    {
        ADCON0 = (ADCON0 & 0b11000011) | ((channel << 2) & 0b00111100);     // set the ADC channel
        
        GO_DONE = 1;                                                        // Start ADC conversion
        while(GO_DONE == 1);                                                // Wait until the ADC conversion is done
        
        adc_value = (unsigned short)((ADRESH << 8) | ADRESL);               // Get the ADC value
    }
    
    return adc_value;
}

//--- PWM Initialization ---------------------------------------------------
void PWM_init(void)
{
    //--- CCP-Module Configurations ------------------------------------
    if(PWM_MODULE_1_ON)
    {
        CCP1CON = 0b00001100;       // Module 1 Mode: PWM mode
    }
    if(PWM_MODULE_2_ON)
    {
        CCP2CON = 0b00001100;       // Module 2 Mode: PWM mode
    }
    
    //--- Timer 2 Configurations ---------------------------------------
    T2CON   = 0b00000101;           // Output Postscale: 1:1; Timer2 On: On; Clock Prescale: Prescaler 4
    PR2     = PR2_VALUE;            // 25kHz / 40us
}

//--- PWM Set --------------------------------------------------------------
void PWM_set(unsigned char module, unsigned short pwm_value)
{
    if(pwm_value > MAX_PWM_VALUE)       // check if PWM value is to high
    {
        pwm_value = MAX_PWM_VALUE;      // set the PWM value to the max value
    }
    
    if((module & 0x01) && PWM_MODULE_1_ON)
    {
        DC1B0   = (__bit)        (pwm_value &  0x01);    // PWM-Bit 0
        DC1B1   = (__bit)        (pwm_value &  0x02);    // PWM-Bit 1
        CCPR1L  = (unsigned char)(pwm_value >> 2);       // PWM-Bit 2 - 9
    }
    if((module & 0x02) && PWM_MODULE_2_ON)
    {
        DC2B0   = (__bit)        (pwm_value &  0x01);    // PWM-Bit 0
        DC2B1   = (__bit)        (pwm_value &  0x02);    // PWM-Bit 1
        CCPR2L  = (unsigned char)(pwm_value >> 2);       // PWM-Bit 2 - 9
    }
}

//--- TIMER 0 Initialization ----------------------------------------------- 
void TIMER0_init(void)
{
    T0CON   = 0b00100000;   // Timer stopped, 16-bit, input = T0CKI, low to high, no prescaler
    TMR0H   = 0x0B;
    TMR0L   = 0xD1;
    
    TMR0IE  = 0;
    TMR0IF  = 0;
}

//--- TIMER 0 Read ---------------------------------------------------------
unsigned short TIMER0_read(void)
{
    unsigned short timer_value  = 0;
    
    TMR0ON  = 0;        // Stop Timer 0
    
    timer_value = (unsigned short)(TMR0L + (TMR0H << 8));
    
    TMR0H   = 0x00;
    TMR0L   = 0x00;
    
    return timer_value;
}

//--- TIMER 1 Initialization ----------------------------------------------- 
void TIMER1_init(void)
{
    T1CON   = 0b10000011;   // 16-bit read, input = T13CKI, no prescaler, timer OSC disabled, Timer stopped
    TMR1H   = 0x00;
    TMR1L   = 0x00;
    
    TMR1IE  = 0;
    TMR1IF  = 0;
}

//--- TIMER 1 Read ---------------------------------------------------------
unsigned short TIMER1_read(void)
{
    unsigned short timer_value  = 0;
    
    TMR1ON  = 0;        // Stop Timer 1
    
    timer_value = (unsigned short)(TMR1L + (TMR1H << 8));
    
    TMR1H   = 0x00;
    TMR1L   = 0x00;
    
    return timer_value;
}

//--- TIMER 3 Initialization -----------------------------------------------
void TIMER3_init(void)
{
    T3CON   = 0b10011000;   // 16-bit read, prescaler = 8, source = FOSC/4
    TMR3H   = 0x00;
    TMR3L   = 0x00;
    
    TMR3IE  = 1;
    TMR3IF  = 0;
}

//--- TIMER 3 Reset --------------------------------------------------------
void TIMER3_reset(void)
{
    TMR3H   = 0x00;
    TMR3L   = 0x00;
}

//--- TIMER Start ----------------------------------------------------------
void TIMER_start(void)
{
    TMR0ON  = 1;        // start Timer 0
    TMR1ON  = 1;        // start Timer 1
    TMR3ON  = 1;        // start Timer 3
}

// <--- end of file