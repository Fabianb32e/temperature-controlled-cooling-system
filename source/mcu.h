//------------------------------------------------------------------------------
//  File name:      mcu.h
//  
//  Description:
//  This file contains all the definitions and function declarations of the 
//  MCU (PIC18F46K20).
//  
//  Author:         Fabian Bachofen
//------------------------------------------------------------------------------

#ifndef MCU_H
#define	MCU_H

//--- Libraries ----------------------------------------------------------------
#include <xc.h>

#include "global.h"

//--- Definitions --------------------------------------------------------------

//--- Pinout ---------------------------------------------------------------
// ADC:
#define     TEMP_AMBIENT        0       	// RA0 / AN0, Pin 19
#define 	TEMP_SET_1			1			// RA1 / AN1, Pin 20
#define		TEMP_SET_2			2			// RA2 / AN2, Pin 21
#define		TEMP_OBJECT_1		3			// RA3 / AN3, Pin 22
#define		TEMP_OBJECT_2		4			// RA4 / AN4, Pin 24

// IOs:
#define     LED_INIT            LATE0       // Pin 25
#define 	LED_USER_1			LATE1		// Pin 26
#define		LED_USER_2			LATE2		// Pin 27

#define     LED_PWM1_25         LATD3       // Pin 41 
#define     LED_PWM1_50         LATD2       // Pin 40
#define     LED_PWM1_75         LATD1       // Pin 39
#define     LED_PWM1_100        LATD0       // Pin 38

#define     LED_PWM2_25         LATD4       // Pin  2
#define     LED_PWM2_50         LATD5       // Pin  3
#define     LED_PWM2_75         LATD6       // Pin  4
#define     LED_PWM2_100        LATD7       // Pin  5

// PWM:
#define 	FAN_PWM_1			RC2			// Pin 36
#define		FAN_PWM_2			RC1			// Pin 35

// Counter:
#define 	FAN_TACHO_1			RA4			// Pin 23
#define		FAN_TACHO_2 		RC0			// Pin 32

// UART:
#define		MCU_RX				RC7			// Pin 1
#define		MCU_TX				RC6			// Pin 44

// I2C:
#define		MCU_SDA				RC5			// Pin 42
#define		MCU_SCL				RC3			// Pin 37


//--- Clock ----------------------------------------------------------------
#define     FOSC             64000000		// 64 MHz
#define     _XTAL_FREQ       64000000UL     // 64 MHz

//--- UART -----------------------------------------------------------------
#define     UART_BUFFER_SIZE       32       // 32 Characters are buffered

//--- ADC ------------------------------------------------------------------
#define     MAX_ADC_CHANNELS       12       // 0 ... 12 are all the Channels
#define     MAX_ADC_VALUE        1023       // Maximum ADC value

//--- PWM ------------------------------------------------------------------
#define     PWM_MODULE_1_ON         1       // Set to 0 = inactrive; set to 1 = active
#define     PWM_MODULE_2_ON         1       // Set to 0 = inactrive; set to 1 = active

#define     F_PWM               25000       // in Hz --> defined by the fan Manufacturer: NOCTUA
#define     PWM_PRESCALER           4       // Prescaler for Timer 2

#define     PR2_VALUE             159       // see callculation in EQUATION 11-1 of the datasheet
#define     MIN_PWM_VALUE           0
#define     MAX_PWM_VALUE         640       // 2^(Resolutions) --> Resolution: see in EQUATION 11-4 of the datasheet

//--- Variables and Arrays -----------------------------------------------------

//--- UART -----------------------------------------------------------------
char            tx_buffer               [UART_BUFFER_SIZE];
char            rx_buffer               [UART_BUFFER_SIZE];

//--- Function Declarations ----------------------------------------------------
void            CLOCK_init              (void);
void            WATCHDOG_clear          (void);
void            PORT_init               (void);
void            ISR_init                (void);

void            UART_init               (void);
void            UART_send               (void);
void            UART_receive            (void);

void            ADC_init                (void);
unsigned short  ADC_read                (unsigned char channel);

void            PWM_init                (void);
void            PWM_set                 (unsigned char  MODULE, 
                                         unsigned short PWM_VALUE);

void            TIMER0_init             (void);
unsigned short  TIMER0_read             (void);

void            TIMER1_init             (void);
unsigned short  TIMER1_read             (void);

void            TIMER3_init             (void);
void            TIMER3_reset            (void);

void            TIMER_start             (void);

#endif	/* MCU_H */

// <--- end of file